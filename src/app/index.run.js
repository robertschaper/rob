(function() {
  'use strict';

  angular
    .module('minotaur')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, $state, $stateParams, $location) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.baseUrl = 'http://bambooiq.ddns.net';


    var unregister = $rootScope.$on('$stateChangeSuccess', function(event, toState) {
      event.targetScope.$watch('$viewContentLoaded', function () {
        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);
      });
      $rootScope.$state.current = toState;
      $rootScope.specialClass = toState.specialClass;
    });
    $rootScope.$on('$destroy', unregister);
    $rootScope.$on('$stateChangeStart', function(event, newState) {
        if (!firebase.auth().currentUser && newState.allowAnonymous !== true) {

            //Don't use: $state.go('login');
            //Apparently you can't set the $state while in a $state event.
            //It doesn't work properly. So we use the other way.
            $location.path("/app/pages/login");
        }
    });

    $log.debug('runBlock end');
  }

})();
