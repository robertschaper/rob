(function() {
  'use strict';

  angular
    .module('minotaur')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $translateProvider, $locationProvider, $httpProvider) {

    // using jQuery get cookies
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
          var thecookies = document.cookie.split(';');
          for (var i = 0; i < thecookies.length; i++) {
              var cookie = jQuery.trim(thecookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) === (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');


    // Add xsrf defaults for django post actions
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.BASE_API = 'http://bambooiq.ddns.net'


    // Custom header
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Authorization'] = 'token 03b70c80adbc4bb820390d7455c4fc916cca6b41';
    // $httpProvider.defaults.headers.common['X-CSRFToken'] = csrftoken
    console.log('cookies.csrftoken: ', csrftoken);


    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyAsGbTLqkn2opdYZ-hf9LhfTcwXsycKpP4",
      authDomain: "bambooiq.firebaseapp.com",
      databaseURL: "https://bambooiq.firebaseio.com",
      storageBucket: "firebase-bambooiq.appspot.com",
      messagingSenderId: "541337408115"
    };
    firebase.initializeApp(config);

    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    // angular-language
    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/languages/',
      suffix: '.json'
    });
    $translateProvider.useLocalStorage();
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy(null);

    // remove ! hash prefix
    $locationProvider.hashPrefix('');

  }

})();
