(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('minotaurHeader', minotaurHeader)
    .factory('NotificationService', ['$resource', '$rootScope', function ($resource, $rootScope) {
      return $resource($rootScope.baseUrl+'/notifications/?format=json',{},{
        get: {
          method: 'GET'
        }
      })
    }]);


  /** @ngInject */
  function minotaurHeader($window, $timeout) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/partials/header/header.html',
      controller: ['NotificationService', '$location', 'firebase', HeaderController],
      controllerAs: 'header',
      bindToController: true,
      link: function (scope, element) {
        var app = angular.element('.appWrap'),
            $el = angular.element(element),
            w = angular.element($window);

        function toggleStickyHeader(){
          if (!$el.hasClass('force-header-sm') && !$el.hasClass('header-aside')){
            if(!$el.hasClass('header-sm') && $window.pageYOffset > 10){
              $el.addClass('header-sm');
              app.addClass('header-sm animate');
              $timeout(function(){
                app.removeClass('animate');
              }, 400);
            } else if($el.hasClass('header-sm') && $window.pageYOffset <= 10){
              $el.removeClass('header-sm');
              app.addClass('animate');
              app.removeClass('header-sm');
              $timeout(function(){
                app.removeClass('animate');
              }, 400);
            }
          }
        }

        w.bind('scroll', toggleStickyHeader);

        function setViewport(){
          if($window.innerWidth < 768){
            $el.addClass('viewport-sm');
            app.addClass('viewport-sm');
          } else {
            $el.removeClass('viewport-sm');
            app.removeClass('viewport-sm');
          }
        }

        setViewport();

        w.bind('resize', setViewport);
      }
    };

    return directive;




    /** @ngInject */
    function HeaderController(NotificationService, $location, firebase){
      var vm = this;

      vm.user = firebase.auth().currentUser;

      /**
       * * Handles the sign in button press.
       * */
      function toggleSignOut() {
        console.log('toggleSignOut')
        if (firebase.auth().currentUser) {

          firebase.auth().signOut();
          $location.path('/app/pages/login');

        } else {
          // Shouldn't be able to sign in here...
        }
      }

      document.getElementById('header-nav-logout').addEventListener('click', toggleSignOut, false);

      NotificationService.get().$promise.then(function (response) {

       var notes =  response.results;
       vm.notes = notes

      }, function (error) {
        alert(error);

      });

    }
  }

})();
