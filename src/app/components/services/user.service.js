(function () {
    'use strict';

    var token = {
      headers:{
          'Authorization': 'token ' + '6fe2a560d7d1e93ce0c5da33eeca7fadd7bc41c8',
          'Content-Type': 'application/json'
      }
    }

    angular
        .module('minotaur')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get('http://localhost:4000/#/profiles', token).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            var id_to_return = $http.get('http://localhost:4000/profiles' + id, token).then(handleSuccess, handleError('Error getting user by id'));
            console.log('id to return: ', id_to_return)
            return id_to_return;
        }

        function Create(user) {
            return $http.post('http://localhost:4000/profiles', user, token).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.put('http://localhost:4000/profiles' + user.id, user, token).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.delete('http://localhost:4000/profiles' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            console.log('print res: ', res)
            return res.data;
        }

        function handleError(error) {
            console.log('handleError: ', error)
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
