(function() {
  'use strict';

  angular
    .module('minotaur')
    .factory('Profile', ['$resource', function ($resource) {
      return $resource('http://bambooiq.ddns.net/users/?format=json',{},{
            get: {
                method: 'GET',
                headers: {
                  'Authorization': 'token ' + '03b70c80adbc4bb820390d7455c4fc916cca6b41'
                }
              }
            });
    }])
    .controller('PagesProfileController', ['$scope', 'Profile', PagesProfileController]);

  /** @ngInject */
  function PagesProfileController($scope, Profile) {

    var vm = this;

    Profile.get(1).$promise.then(function (response) {

      $scope.profile  =  response.results[0];
      console.log($scope.profile)

      }, function (error) {
        alert(error);

      });


  }


})();
