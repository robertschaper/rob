(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ForgotPasswordController', ['firebase', ForgotPasswordController]);

  /** @ngInject */
  function ForgotPasswordController() {

    /**
     * Sends a password reset email to the registered user
     * **/
    function sendPasswordReset() {
      var email = document.getElementById('email').value;

      firebase.auth().sendPasswordResetEmail(email).then(function() {
        // Password Reset Email Sent!

        alert('Password Reset Email Sent!');

      }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;

        if (errorCode == 'auth/invalid-email') {
          alert(errorMessage);
        } else if (errorCode == 'auth/user-not-found') {
          alert(errorMessage);
        }
        $log.debug(error);

      });

    }

    document.getElementById('quickstart-reset-password').addEventListener('click', sendPasswordReset, false);


  }

})();
