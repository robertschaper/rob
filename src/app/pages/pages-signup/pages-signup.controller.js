(function() {
    'use strict';
    angular
        .module('minotaur')

      .factory('CreateUser', ['$resource', function ($resource) {

        return $resource('http://bambooiq.ddns.net/users',{},{
          save: {
            method: 'POST'
          }});
      }])
      .controller('SignupController', ['CreateUser', '$scope', '$http', SignupController]);

  /** @ngInject */
  function SignupController(CreateUser, $scope, $http) {

    /**.eslintrc
     * Handles the sign up button press.
     */
    function handleSignUp() {
      var userName = document.getElementById('userName').value;
      var fullName = document.getElementById('fullName').value;
      var email = document.getElementById('email').value;
      var password = document.getElementById('password').value;
      if (email.length < 4) {
        alert('Please enter an email address.');
        return;
      }
      if (password.length < 4) {
        alert("Please enter a password.");
        return;
      }
      // Sign in with email and pass.

      var createdUser = firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function (firebaseUser) {
          console.log("firebase: ", firebase)
          console.log("firebaseUser: ", firebaseUser)
          console.log("Received: User " + firebaseUser.uid + " created successfully!")
          console.log("Now create the profile in mysql");

          console.log(firebaseUser)
          var userToInsert = {};
          //console.log('uid: ' + toString(firebaseUser.uid));
          userToInsert.id = parseInt((Math.random()*10)*Math.random()*(Math.random()*100));
          userToInsert.email = firebaseUser.email;
          userToInsert.password = firebaseUser.uid;
          userToInsert.username = userName;
          // userToInsert.created_at = new Date();

          var json_user = JSON.stringify(userToInsert);
          console.log("json_user: ", json_user)

          $http.post("http://bambooiq.ddns.net/users/", json_user, {})
            .then(function (response) {
              console.log(response);
              return response;
            });

          return firebaseUser;
        })

        .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;

        if (errorCode === 'auth/weak-password') {
          alert('The password is too weak.');
        } else if (errorCode === 'auth/email-already-in-use') {
          alert('Email address already in use.');
        } else if (errorCode === 'auth/invalid-email') {
          alert('Email address already in use.');
        } else {
          alert(errorMessage);
        }

      });

    }

      document.getElementById('quickstart-sign-up').addEventListener('click', handleSignUp, false);

  }

})();
