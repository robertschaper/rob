(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('LoginController', ['$location', '$scope', '$firebaseAuth', LoginController]);

  /** @ngInject */
  function LoginController($location, $scope, $firebaseAuth) {

    /**
     * * Handles the sign in button press.
     * */
    function toggleSignIn() {

      var email = document.getElementById('email').value;
      var password = document.getElementById('password').value;
      if (email.length < 4) {
        alert('Please enter an email address.');
        return;
      }
      if (password.length < 4) {
        alert('Please enter a password.');
        return;
      }

      // Sign in with email and pass.
      $scope.authObj.$signInWithEmailAndPassword(email, password).then(function (firebaseUser) {
        console.log("Signed in as: ", firebaseUser.uid)
      }).catch(function (error) {
        console.log("Authentication failed: ", error)
      })

    }



    /**
     * initApp handles setting up UI event listeners and registering Firebase auth listeners:
     *  - firebase.auth().onAuthStateChanged: This listener is called when the user is signed in or
     *    out, and that is where we update the UI.
     */
    function initApp() {

      $scope.authObj = $firebaseAuth()

      // Listening for auth state changes.
      $scope.authObj.$onAuthStateChanged(function (user) {

        if (user) {
          // User is signed in.
          var displayName = user.displayName;
          $scope.email = user.email;
          var emailVerified = user.emailVerified;
          var photoURL = user.photoURL;
          var isAnonymous = user.isAnonymous;
          var uid = user.uid;
          var providerData = user.providerData;
          $scope.token = user.token;

          // Used to show user details during debug
          // document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');

          // Push to /app/dashboard view
          $location.path('/app/dashboard');

        } else {
          // Reactivate the signin listener and button state
          document.getElementById('quickstart-sign-in').disabled = false;
          document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
          $location.path('/app/pages/login').replace();

        }

      });

      document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);

    }
    window.onload = function() {
      initApp();
    }

  }

})();
