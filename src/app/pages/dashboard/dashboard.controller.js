(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('DashboardController', DashboardController)
    .controller('StatisticsController', StatisticsController)
    .controller('RealtimeLoadController', RealtimeLoadController)


  /** @ngInject */
  function DashboardController(moment) {
    var vm = this;

    vm.datePicker = {
      date: {
        startDate: moment().subtract(1, "days"),
        endDate: moment()
      },
      opts: {
        ranges: {
          'This Month': [moment().startOf('month'), moment()],
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'day'), moment().subtract(1, 'day')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left'
      }
    };
  }

  function StatisticsController() {
    var vm = this;

    vm.colors = ['#e05d6f', '#23a9e6'];

    vm.options = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'top',
        labels: {
          boxWidth: 15
        },
        usePointStyle: false
      },
      tooltips: {
        titleSpacing: 10,
        titleMarginBottom: 10,
        bodySpacing: 8,
        cornerRadius: 3,
        xPadding: 15,
        yPadding: 15
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontFamily: '"Raleway", "Arial", sans-serif',
            fontSize: 13,
            fontStyle: 'bold',
            fontColor: '#878787'
          }
        }],
        yAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontFamily: '"Raleway", "Arial", sans-serif',
            fontSize: 13,
            fontStyle: 'bold',
            fontColor: '#878787'
          }
        }]
      }
    };

    vm.labels = ['2/13', '2/14', '2/15', '2/16', '2/17', '2/18', '2/19', '2/20', '2/21', '2/22', '2/23', '2/24'];
    vm.data = [
      [14, 40, 35, 39, 42, 50, 46, 49, 59, 60, 58, 74],
      [50, 80, 90, 85, 99, 125, 114, 96, 130, 145, 139, 160]
    ];
    vm.datasetOverride = [{
      label: "Unique Members",
      borderColor: '#e05d6f',
      backgroundColor: 'rgba(224,93,111,0.2)',
      borderWidth: 3,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      type: 'line',
      pointHoverBorderColor: "#e05d6f",
      pointHoverBackgroundColor: "#e05d6f"
    }, {
      label: "Total Tweets Recieved",
      backgroundColor: '#23a9e6',
      hoverBackgroundColor: '#057cb2',
      borderWidth: 0,
      type: 'bar'
    }];
  }

  function RealtimeLoadController($scope, $interval) {
    /* eslint-disable */
    $scope.options = {
      renderer: 'area',
      height: 133
    };

    $scope.seriesData = [[], []];
    var random = new Rickshaw.Fixtures.RandomData(50);

    for (var i = 0; i < 50; i++) {
      random.addData($scope.seriesData);
    }

    var updateInterval = 800;

    $interval(function() {
      random.removeData($scope.seriesData);
      random.addData($scope.seriesData);
      for (var i = 0; i < $scope.series.length; i++) {
        var name = $scope.series[i].name;
        var color = $scope.series[i].color;
        var data = $scope.seriesData[i];
        $scope.series[i] = {
          name: name,
          color: color,
          data: data
        };
      }
    }, updateInterval);

    $scope.series = [{
      name: 'Series 1',
      color: 'steelblue',
      data: $scope.seriesData[0]
    }, {
      name: 'Series 2',
      color: 'lightblue',
      data: $scope.seriesData[1]
    }];

    $scope.features = {
      hover: {
        xFormatter: function(x) {
          return new Date(x * 1000).toUTCString();
        },
        yFormatter: function(y) {
          return Math.floor(y) + '%';
        }
      }
    };
    /* eslint-enable */

  }

})();
